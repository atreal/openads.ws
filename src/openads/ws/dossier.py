"""."""

import json
import logging
import re

from openads.ws.demandeur import Demandeur
from openmairie.ws.base import OMBaseObject
from openmairie.ws.base import extract_query_string
from flatten_json import flatten
from datetime import date

log = logging.getLogger(__name__)

# Used in add_pieces, if no Nature is extracted nor provided
DEFAULT_PIECE_NATURE = 1


class Dossier(OMBaseObject):
    """."""

    demande_fields_mapping = {
        "collectivite": "om_collectivite",
        "terrain.numero_voie": "terrain_adresse_voie_numero",
        "terrain.nom_voie": "terrain_adresse_voie",
        "terrain.complement": "complement",
        "terrain.lieu_dit": "terrain_adresse_lieu_dit",
        "terrain.localite": "terrain_adresse_localite",
        "terrain.code_postal": "terrain_adresse_code_postal",
        "terrain.boite_postale": "terrain_adresse_bp",
        "terrain.cedex": "terrain_adresse_cedex",
    }

    d_t_fields_mapping = {
        "engagement.lieu": "enga_decla_lieu",
        "engagement.date": "enga_decla_date",
    }

    output_fields_mapping = {
        "etat": "etat",
        "date_depot": "date_depot",
        "date_limite": "date_limite_instruction",
        "date_decision": "date_decision",
        "avis_decision": "decision",
    }

    # XXX should move in a config file
    dossier_conf = {
        "DIA": {"dossier_autorisation_type_detaille_id": 400, "demande_type_id": 150},
        "CUa": {"dossier_autorisation_type_detaille_id": 6, "demande_type_id": 61},
        "CUb": {"dossier_autorisation_type_detaille_id": 9, "demande_type_id": 60},
        "DP": {"dossier_autorisation_type_detaille_id": 420, "demande_type_id": 200},
        "DPL": {"dossier_autorisation_type_detaille_id": 421, "demande_type_id": 208},
        "DPMI": {"dossier_autorisation_type_detaille_id": 422, "demande_type_id": 216},
        "PC": {"dossier_autorisation_type_detaille_id": 2, "demande_type_id": 41},
        "PI": {"dossier_autorisation_type_detaille_id": 1, "demande_type_id": 58},
    }

    def get(self, data):
        """."""
        html = self._get("dossier_instruction", data["id"])
        if not html:
            return

        mapped_data = {}
        for k, v in list(self.output_fields_mapping.items()):
            try:
                value = html.select("#%s" % k)[0].contents
                if len(value) == 0:
                    mapped_data[v] = u""
                else:
                    mapped_data[v] = value[0]
            except IndexError:
                raise RuntimeError

        return mapped_data

    def set(self, data):
        """."""
        mapped_data = {}

        # Dossier type choice : XXX harcoded for open
        # dossier_type = "PC"
        # dossier_type = data.pop("dossier_type")

        # If we have a type_detaille (ex: CU -> CUa), we override the basetype
        # if "type_detaille" in data:
        dossier_type = data.pop("type_detaille")

        mapped_data["dossier_autorisation_type_detaille"] = self.dossier_conf[
            dossier_type
        ]["dossier_autorisation_type_detaille_id"]
        mapped_data["demande_type"] = self.dossier_conf[dossier_type]["demande_type_id"]

        # Date
        if "date_demande" in data:
            mapped_data["date_demande"] = data.pop("type_detaille")
        else:
            mapped_data["date_demande"] = date.today().strftime("%d/%m/%Y")

        # Demandeurs
        demandeur_ids = []
        mandataire_ids = []
        dmdr = Demandeur(self.backend)
        for demandeur in data.pop("demandeurs"):
            demandeur["collectivite"] = data["collectivite"]
            demandeur_ids.append(dmdr.set(demandeur))

        mapped_data["petitionnaire_principal[]"] = demandeur_ids.pop(0)["id"]
        if len(demandeur_ids):
            mapped_data["petitionnaire[]"] = [d["id"] for d in demandeur_ids]

        if "mandataires" in data:

            for mandataire in data.pop("mandataires"):
                mandataire["collectivite"] = data["collectivite"]
                mandataire_ids.append(dmdr.set(mandataire))

            if len(mandataire_ids):
                mapped_data["delegataire[]"] = [d["id"] for d in mandataire_ids]

        # References cadastrales
        textual_references = []
        for ref in data["terrain"].pop("references_cadastrales"):
            # numero should be on 4 digits
            ref["numero"] = ref["numero"].zfill(4)
            textual_references.append(ref["prefixe"] + ref["section"] + ref["numero"])
        # Bad implementation in openADS, refs should end with a ';'
        mapped_data["terrain_references_cadastrales"] = (
            ";".join(textual_references) + ";"
        )

        # Demande fields
        for key, value in list(flatten(data, ".").items()):
            if key in self.demande_fields_mapping:
                mapped_data[self.demande_fields_mapping[key]] = value

        log.debug("POST %s" % mapped_data)
        html = self._post("demande_nouveau_dossier", mapped_data)

        if not html:
            return
        try:
            numero_dossier = html.select("#new_di")[0].contents[0].replace(" ", "")
            recepisse_link = (
                self.backend["url"]
                + html.select("#link_demande_recepisse")[0].attrs["href"][2:]
            )
            filename, content_type, content = self._get_file(recepisse_link)
        except:
            return

        if "engagement" in data:
            # We should retrieve the initialized Donnees Techniques
            # first implementation
            # html = self._get("dossier_instruction", obj_id=numero_dossier, action=100)
            # d_t_id = extract_query_string(
            #     str(html.select("#form-container script")[0].contents[0]).split("'")[3]
            # )["idx"]
            # mapped_data["donnees_techniques"] = d_t_id
            # mapped_data["dossier_instruction"] = numero_dossier

            # Second one, wich include all d_t in a hidden input values
            html = self._get("dossier_instruction", obj_id=numero_dossier, action=3)
            mapped_data = json.loads(
                str(html.select("input#initial_dt")[0].attrs["value"]).replace("'", '"')
            )

            # Donnees Techniques fields
            for key, value in list(flatten(data, ".").items()):
                if key in self.d_t_fields_mapping:
                    if isinstance(value, date):
                        mapped_data[self.d_t_fields_mapping[key]] = value.strftime(
                            "%d/%m/%Y"
                        )
                    else:
                        mapped_data[self.d_t_fields_mapping[key]] = value

            # When we extract Donnees Techniques from `input #initial_dt`,
            # id is set to previous record (?)
            mapped_data["donnees_techniques"] = str(
                int(mapped_data["donnees_techniques"]) + 1
            )
            mapped_data["dossier_instruction"] = numero_dossier
            mapped_data["dossier_autorisation"] = ""

            html = self._update(
                "donnees_techniques",
                numero_dossier,
                mapped_data,
                module="sousform",
                action=1,
                url_vars={"idx": mapped_data["donnees_techniques"]},
            )

        return {
            "numero_dossier": numero_dossier,
            "filename": filename,
            "content_type": content_type,
            "content": content,
        }

    def add_files(self, dossier_id, files):
        """."""
        mapped_data = {
            "dossier": dossier_id,
            "date_creation": date.today().strftime("%d/%m/%Y"),
        }

        result = []
        for filedata in files:
            uid = self._set_file(
                filedata["filename"], filedata["content"], filedata["content_type"]
            )
            if not uid:
                log.error("POST %%" % mapped_data)
                return

            mapped_data["uid"] = uid
            mapped_data["uid_upload"] = filedata["filename"]

            # XXX from config
            mapped_data["document_numerise_type"] = 21

            # XXX - I have no clue about what should be returned
            html = self._update(
                "document_numerise", dossier_id, mapped_data, module="sousform"
            )
            result.append({"filename": filedata["filename"], "status": "OK"})

        return result

    def add_pieces(self, dossier_id, pieces):
        """."""
        mapped_data = {
            "dossier": dossier_id,
        }

        document_numerise_type = self.get_reqmo("document_numerise_type", ["code"])
        if not document_numerise_type:
            return

        document_types = {
            item["code"]: item["document_numerise_type"]
            for item in document_numerise_type
        }

        add_piece_form = self._get(
            "document_numerise",
            module="sousform",
            action=0,
            url_vars={"idxformulaire": dossier_id, "retourformulaire": "dossier_instruction"},
        )
        try:
            suggested_reception_date = add_piece_form.select("#content #date_creation")[
                0
            ].attrs["value"]
        except KeyError:
            log.warning(
                "Could not extract `date_creation`. "
                "Today's date will be used if no reception date is provided"
            )
            suggested_reception_date = date.today().strftime("%d/%m/%Y")

        try:
            suggested_nature = add_piece_form.select(
                "#content #document_numerise_nature"
            )[0].attrs["value"]
        except KeyError:
            log.warning(
                "Could not extract `document_numerise_nature`. "
                "`initiale` value will be used"
            )
            suggested_nature = DEFAULT_PIECE_NATURE

        result = []
        for piece in pieces:
            # Did the consumer provide a reception date ?
            # If not, we'll use the openADS suggested reception date
            mapped_data["date_creation"] = piece.get(
                "reception_date", suggested_reception_date
            )

            mapped_data["document_numerise_nature"] = piece.get(
                "nature", suggested_nature
            )

            uid = self._set_file(
                piece["filename"], piece["content"], piece["content_type"]
            )
            if not uid:
                log.error("POST %%" % mapped_data)
                return

            mapped_data["uid"] = uid
            mapped_data["uid_upload"] = piece["filename"]
            mapped_data["document_numerise_type"] = document_types[
                str(piece["piece_type"])
            ]
            mapped_data["description_type"] = piece.get("custom_piece_type", "")

            html = self._update(
                "document_numerise", dossier_id, mapped_data, module="sousform"
            )

            # XXX: to complete the response, reception date can be retrieved via :
            # html.select("#content #date_creation")
            result.append({"filename": piece["filename"], "status": "OK"})

        return result

    def get_courrier(self, dossier_id, courrier_type):
        """."""
        url_vars = {
            "retourformulaire": "dossier_instruction",
            "idxformulaire": dossier_id,
        }
        html = self._get("instruction", dossier_id, module="soustab", url_vars=url_vars)
        if not html:
            return
        # We suppose there  is only one courrier with this courrier_type
        try:
            idx = re.search(
                "idx=\d*", html.find_all("a", string=courrier_type)[0].attrs["onclick"]
            ).group()
        except IndexError:
            # There is no courrier of this type
            return

        instruction = idx.split("=")[1]

        courrier_url = (
            "%s/app/index.php?module=sousform&obj=instruction&idx=%s&action=120&idxformulaire=%s"
            % (self.backend["url"], instruction, dossier_id)
        )

        filename, content_type, content = self._get_file(courrier_url)

        return {"filename": filename, "content_type": content_type, "content": content}

    def legacy_get(self, data):
        """dossier_instruction legacy wrapper.

        This method wraps the RESTLER legacy webservice used by IsiGeo.
        Because we don't want waste time for this out-of-date and deprecated WS,
        we just call the legacy WS and return the originalm JSON object
        """
        json_data = self._legacy_get("dossier_instructions", data["id"])
        if not json_data:
            return

        return json_data

    def dossier_exists(self, dossier_id: str) -> bool:
        """."""
        # http://localhost/openads/app/index.php?module=form&obj=demande_dossier_encours&action=200&idx_dossier=AT0130551200001P0
        html = self._get(
            "demande_dossier_encours",
            action="200",
            url_vars={"idx_dossier": dossier_id},
        )

        if not html:
            return

        try:
            content = json.loads(html.select("#form-container")[0].contents[0])
        except KeyError:
            return

        if "dossier_exists" in content:
            return {"existe": content["dossier_exists"]}
        else:
            return

    def depot_possible(self, dossier_id: str, type_demande: str) -> bool:
        """."""
        # http://localhost/openads/app/index.php?module=form&obj=demande_dossier_encours&action=200&idx_dossier=AT0130551200001P0&type_demande=daact
        html = self._get(
            "demande_dossier_encours",
            action="200",
            url_vars={"idx_dossier": dossier_id, "type_demande": type_demande},
        )

        if not html:
            return

        try:
            content = json.loads(html.select("#form-container")[0].contents[0])
        except KeyError:
            return

        if "dossier_exists" in content and "possible_demande_type" in content:
            return {
                "depot_possible": content["dossier_exists"]
                and content["possible_demande_type"]
            }
        else:
            return
