"""."""

import logging

from datetime import date

from openmairie.ws.base import OMBaseObject

log = logging.getLogger(__name__)


class Instruction(OMBaseObject):
    """."""

    def create(
        self,
        evenement: str,
        dossier_id: str,
        lettre_type: bool = False,
        event_date: str = None,
    ) -> str:
        """."""
        # def _post(self, obj, data, module="form", action=0, url_vars=None):
        if event_date is None:
            event_date = date.today().strftime("%d/%m/%Y")
        # we allow an integer or a string. i
        # integer -> evenvenement id
        # string -> evenement name
        try:
            evenement_id = int(evenement)
        except ValueError:
            evenement_id = self._get_evenement_by_name(evenement)
        if evenement_id is None:
            return

        data = {
            "evenement": evenement_id,
            "date_evenement": event_date,
            # "signataire_arrete": "16",
            "dossier": dossier_id,
        }
        url_vars = {
            "idxformulaire": dossier_id,
        }
        html = self._post("instruction", data, module="sousform", url_vars=url_vars)

        # We are looking for at least one valid confirmation message and no error message
        if (
            len(html.select(".message.ui-state-valid")) < 1
            or len(html.select(".message.ui-state-error")) != 0
        ):
            raise RuntimeError

        try:
            instruction_id = html.select("#instruction")[0].attrs["value"]
        except IndexError:
            raise RuntimeError

        if not lettre_type:
            return instruction_id

        #  Let's finalize the Edition
        data = {"submit": "plop"}
        url_vars = {
            "idx": instruction_id,
            "idxformulaire": dossier_id,
        }

        html = self._post(
            "instruction", data, module="sousform", action="100", url_vars=url_vars
        )

        # Then apply date_retour_signature
        data = {
            "type_mise_a_jour": "date_retour_signature",
            "date": event_date,
            "code_barres": "11" + instruction_id.zfill(10),
            "is_valid": "true",
            "validation": "Valider",
        }
        html = self._post(
            "instruction_suivi_mise_a_jour_des_dates",
            data,
            action="170",
            module="form"
        )

        # We are looking for at least one valid confirmation message and no error message
        if (
            len(html.select(".message.ui-state-valid")) < 1
            or len(html.select(".message.ui-state-error")) != 0
        ):
            raise RuntimeError

        return instruction_id

    def update(self, instruction: str, dossier_id: str, event_date: str = None) -> bool:
        """."""
        pass

    def _get_evenement_by_name(self, name: str) -> str:
        """."""
        data = {
            "recherche": name,
            "selectioncol": "1",
        }
        html = self._post("evenement", data, module="tab")
        if len(html.select("table.tab-tab tr.tab-data.empty")) == 1:
            # No result
            return

        line = html.select("table.tab-tab tr.tab-data")
        if len(line) != 1:
            raise ValueError

        return line[0].select("td.col-0")[0].text
