"""."""

import logging
import re
import json
import tempfile

from openads.ws.demandeur import Demandeur
from openmairie.ws.base import OMBaseObject
from flatten_json import flatten
from datetime import date
from zipfile import ZipFile

log = logging.getLogger(__name__)


class PlatauTask(OMBaseObject):
    """."""

    def search(
        self,
        category: str = None,
        state: str = None,
        typology: str = None,
        stream: str = None,
        numero_dossier: str = None,
        external_uid: str = None,
        external_uid_type: str = "dossier",
    ):
        """."""
        url_vars = {}

        if category:
            url_vars["category"] = category

        if state:
            url_vars["state"] = state

        if typology:
            url_vars["type"] = typology

        if stream:
            url_vars["stream"] = stream

        if numero_dossier:
            url_vars["dossier"] = numero_dossier

        if external_uid:
            url_vars["external_uid"] = external_uid
            url_vars["object"] = external_uid_type

        if external_uid_type:
            url_vars["object"] = external_uid_type

        # When providing obj_id="]", openADS retuns a tasks list
        html = self._get("task", obj_id="]", action="998", url_vars=url_vars)

        if not html:
            return
        try:
            tasks = json.loads(html.select("#form-container")[0].contents[0])
        except KeyError:
            return

        #  No results
        if not tasks:
            return []

        return [task for task in tasks.values()]

    def get(self, task_id):
        """."""
        html = self._get("task", obj_id=task_id, action="998")

        if not html:
            return

        try:
            task = json.loads(html.select("#form-container")[0].contents[0])
        except KeyError:
            return

        return task

    def update(self, task_id, state=None, external_uid=None):
        """."""
        # data = {"valid": "true"}
        data = {}

        if state:
            data["state"] = state

        if external_uid:
            data["external_uid"] = external_uid

        html = self._update("task", task_id, data, action="997")

        if not html:
            return

        return True

    def create(self, category, typology, json_payload):
        """."""
        html = self._post(
            "task",
            {"category": category, "type": typology, "json_payload": json_payload},
            action="996",
        )

        # We are looking for at least one valid confirmation message and no error message
        if (
            len(html.select(".message.ui-state-valid")) < 1
            or len(html.select(".message.ui-state-error")) != 0
        ):
            raise RuntimeError

        return True

    def get_file(self, task_id):
        """."""
        task = self.get(task_id)

        if task is None:
            return

        # file_provider_key must be exclusive
        keys = set(task.keys())
        file_provider_keys = {"document_numerise", "instruction", "consultation"}
        if len(keys.intersection(file_provider_keys)) != 1:
            return

        file_name = ""
        file_path = None
        if "document_numerise" in task:
            file_path = task["document_numerise"]["path"]

        elif "instruction_notification" in task:

            # We may have multiple files to download
            if "annexes" in task["instruction_notification"]:
                file_path = []
                for annexe in task["instruction_notification"]["annexes"]:
                    for key, value in annexe.items():
                        if key.startswith("path"):
                            file_path.append(value)
                            break
                if len(file_path) >= 1:
                    # There is some Annexe
                    file_path.append(task["instruction_notification"]["path"])
                    file_name = f"{task_id}_archive.zip"
                else:
                    file_path = task["instruction_notification"]["path"]

            else:
                file_path = task["instruction_notification"]["path"]

        elif "instruction" in task:

            file_path = task["instruction"]["path"]

        elif "consultation" in task:
            file_path = task["consultation"]["path_om_fichier_consultation"]

        else:
            log.warn("Task %s did not provide any file related property")
            return

        if not isinstance(file_path, list):
            # One and only file to handle
            file_url = self.backend["url"] + "/" + file_path
            file_name, content_type, content = self._get_file(file_url)

            return {"filename": file_name, "content_type": content_type, "content": content}

        else:
            # Multiple files : we'll pack these in zip file.
            with tempfile.NamedTemporaryFile(delete=False) as f:
                zip_file_path = f.name

            zip_file = ZipFile(zip_file_path, "w")

            for path in file_path:

                file_url = self.backend["url"] + "/" + path
                filename, content_type, content = self._get_file(file_url)
                zip_file.writestr(filename, content)
                log.info(
                    f"Retrieved  {filename} and write it to archive {zip_file_path}"
                )
            zip_file.close()

            with open(zip_file_path, 'rb') as fd:
                content = fd.read()

            return {"filename": file_name, "content_type": "application/zip", "content": content}
