"""."""

from builtins import str
import logging

from openmairie.ws.base import OMBaseObject
from bs4 import BeautifulSoup
from flatten_json import flatten, unflatten
from datetime import date

log = logging.getLogger(__name__)


class Demandeur(OMBaseObject):
    """
    """

    input_fields_mapping = {
        "collectivite": "om_collectivite",
        "type_personne": "qualite",
        "typologie": "type_demandeur",
        "denomination": "personne_morale_denomination",
        "raison_sociale": "personne_morale_raison_sociale",
        "adresse.numero_voie": "numero",
        "adresse.nom_voie": "voie",
        "adresse.complement": "complement",
        "adresse.lieu_dit": "lieu_dit",
        "adresse.localite": "localite",
        "adresse.code_postal": "code_postal",
        "adresse.boite_postale": "bp",
        "adresse.cedex": "cedex",
        "coordonnees.email": "courriel",
        "coordonnees.telephone": "telephone_fixe",
        "coordonnees.indicatif": "indicatif",
    }

    output_fields_mapping = {v: k for k, v in list(input_fields_mapping.items())}

    def get(self, petitionnaire_id):
        """."""
        html = self._get("petitionnaire", petitionnaire_id)
        if not html:
            return

        mapped_data = {}

        def drop_linebreak(value):
            if value.endswith("\n"):
                return value[:-1]

        # Specific case for nom prenom
        type_personne = drop_linebreak(html.select("#qualite")[0].contents[0])
        self.output_fields_mapping.update(
            {"%s_nom" % type_personne: "nom", "%s_prenom" % type_personne: "prenom"}
        )

        for k, v in list(self.output_fields_mapping.items()):

            try:
                node = html.select("#%s" % k)[0].contents
                if len(node) == 0:
                    mapped_data[v] = u""
                else:
                    mapped_data[v] = drop_linebreak(node[0])
            except IndexError:
                raise RuntimeError

        # unflatten does not support correctly unicode
        return unflatten({str(k): str(v) for k, v in list(mapped_data.items())}, ".")

    def set(self, data):
        """."""
        mapped_data = {}

        for key, value in list(flatten(data, ".").items()):

            # Specific case for nom prenom
            if key in ("nom", "prenom"):
                remote_key = "%s_%s" % (data["type_personne"].replace(" ", "_"), key)
                mapped_data[remote_key] = value

            # generic case
            else:
                remote_key = self.input_fields_mapping[key]
                mapped_data[remote_key] = value

        # We allow email notification
        mapped_data["notification"] = "Oui"

        log.debug("POST %s" % mapped_data)
        html = self._post(mapped_data["type_demandeur"], mapped_data)
        if not html:
            return

        try:
            petitionnaire_id = html.select("#demandeur")[0].attrs["value"]
        except:
            return

        if petitionnaire_id == "":
            raise RuntimeError

        return {"id": petitionnaire_id}
