openads.ws
==========

.. image:: https://img.shields.io/pypi/v/openads.ws.svg
    :target: https://pypi.python.org/pypi/openads.ws
    :alt: Latest PyPI version

.. image:: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal.png
   :target: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal
   :alt: Latest Travis CI build status

openADS WS wrappers

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`openads.ws` was written by `atReal <contact@atreal.fr>`_.
