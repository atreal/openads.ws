from setuptools import setup, find_packages


setup(
    name="openads.ws",
    version="1.0.0",
    url="https://gitlab.com/atreal/openads.ws",
    author="atReal",
    author_email="contact@atreal.fr",
    description="openADS WS wrappers",
    long_description=open("README.rst").read(),
    packages=find_packages("src"),
    package_dir={"": "src"},
    namespace_packages=["openads"],
    install_requires=["openmairie.ws", "bs4", "flatten_json"],
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
    ],
)
